ReadMe del progetto di PR2 scritto da Pau Elisa e Pintus Alessandro.

Lo scopo del progetto è l'integrazione delle classi Java nell'ambiente Libre Office attraverso la mediazione di Obba. 

Sono stati creati tre fogli elettronici; ognuno contiene il test di tre funzioni Libre Office, nell'ordine: semplice, complessa e custom. 
La prima consiste in una nuova implementazione della funzione PI.GRECO, la quale restituisce il valore del pi greco con 14 cifre sgnificative. 
La seconda reimplementa SOMMA.SOMMA.Q, che restituisce la somma delle somme dei quadrati dei valori di ciascuna delle due matrici prese in input. 
La terza, con l'ausilio delle api di html unit, accede a un sito specificato dall'utente e restituisce alcune informazioni quali il titolo, il contenuto dell'html, e la possibilità di accesso.

Il programma è stato testato su Ubuntu Linux 14.04, jdk 1.7.0_55, LibreOffice 4.4.3, Obba 4.2.2.

Per compilare(dalla cartella java):
[percorso cartella java 7]/bin/javac -cp ../libs it/unica/pr2/progetto2015/g48864_48917/*.java it/unica/pr2/progetto2015/interfacce/*.java

Per creare il jar spostare il contenuto di libs dentro java, eseguire il comando dalla cartella java:
jar cfm ../build/progetto2015.jar manifest.txt ./
e rimettere su libs le cartelle spostate.


