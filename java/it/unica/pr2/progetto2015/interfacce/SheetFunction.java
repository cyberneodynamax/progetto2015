package it.unica.pr2.progetto2015.interfacce;

public interface SheetFunction 
{
    Object execute(Object ... args) ;

    String getCategory();

    String getHelp(); 
     
    String getName();
}
