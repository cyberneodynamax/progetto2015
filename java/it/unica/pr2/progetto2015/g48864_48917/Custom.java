package it.unica.pr2.progetto2015.g48864_48917;

import it.unica.pr2.progetto2015.interfacce.SheetFunction;
import java.util.*;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.html.*;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebClientOptions;
import java.io.IOException;

public class Custom implements SheetFunction
{
    public Custom()
    {
    
    };
    
    public Object execute(Object ... args)
    {
        WebClient webclient = new WebClient();
        WebClientOptions opzioni = webclient.getOptions();
        String[][] anteprimaPagina = new String[6][1];
        
        opzioni.setCssEnabled(false);
        opzioni.setAppletEnabled(false);
        opzioni.setJavaScriptEnabled(false);
        try
        {
            Page pagina = webclient.getPage("http://" + args[0]);
            HtmlPage paginaHtml = HTMLParser.parseHtml(pagina.getWebResponse(),pagina.getEnclosingWindow());
            List<HtmlElement> listaHeader = null, listaParagrafi = null, listaLink = null, listaForm = null;
            HtmlElement body = paginaHtml.getBody();

            anteprimaPagina[0][0] = paginaHtml.getTitleText();         //Titolo pagina
            
            listaHeader = body.getHtmlElementsByTagName("h1");
            if(!listaHeader.isEmpty())
                anteprimaPagina[1][0] = listaHeader.get(0).getTextContent();//Primo h1
            else
                anteprimaPagina[1][0] = "Non presente";
            listaParagrafi = body.getHtmlElementsByTagName("p");
            anteprimaPagina[2][0] = "" + listaParagrafi.size();        //Numero paragrafi
            listaLink = body.getHtmlElementsByTagName("a");
            anteprimaPagina[3][0] = "" + listaLink.size();             //Numero link
            listaForm = body.getHtmlElementsByTagName("form");
            anteprimaPagina[4][0] = "" + listaForm.size();             //Numero form
            anteprimaPagina[5][0] = "No";                              //Non è possibile il login (default)
            if(!listaLink.isEmpty())
            {
                for(HtmlElement elemento : listaLink)               //Controllo link per login
                {
                    String valore = elemento.getTextContent().toLowerCase();
                    if(valore.equals("accedi") || valore.equals("login") || valore.equals("log in") || valore.equals("entra"))
                    {
                        anteprimaPagina[5][0] = "Si";
                        return anteprimaPagina;
                    }     
                }
            }
            if(!listaForm.isEmpty())
            {
            
                if(anteprimaPagina[5][0] != "Si")                      //Non ha trovato un link per il login
                {
                    for(HtmlElement elemento : listaForm)           //Possibilità di accesso
                    {
                        if(elemento.getAttribute("type").equals("password"));
                        {
                            anteprimaPagina[5][0] = "Si";
                            return anteprimaPagina;
                        }
                    }
                }
            }
            return anteprimaPagina;
        }
        catch(IOException e )
        {
             for(int i=0; i<6; i++)
                anteprimaPagina[i][0] = "Pagina non trovata";
             return anteprimaPagina;
        }
    
    }

    public String getCategory()
    {
        return "HTML";
    }

    public String getHelp()
    {
        return "Visualizza il titolo, primo header e alcune informazioni del sito.";
    }
       
    public String getName()
    {
        return "VISUALIZZA.SITO";
    }
}

