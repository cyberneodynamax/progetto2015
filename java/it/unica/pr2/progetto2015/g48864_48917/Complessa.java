package it.unica.pr2.progetto2015.g48864_48917;

import it.unica.pr2.progetto2015.interfacce.SheetFunction;

public class Complessa implements SheetFunction 
{
        public Complessa()
        {
        
        };
 
	public Object execute(Object ... args) 
	{
		Double[][] primaMatrice = (Double[][])args[0];
		Double[][] secondaMatrice = (Double[][])args[1];
		double primaSomma = 0, secondaSomma = 0; 

		for(Double[] riga : primaMatrice)
                {
                    for(double elementoPrimaMatrice : riga)
                        primaSomma+=elementoPrimaMatrice * elementoPrimaMatrice;
                }

		for(Double[] riga : secondaMatrice)
                {
                    for(double elementoSecondaMatrice : riga)
                        secondaSomma+=elementoSecondaMatrice * elementoSecondaMatrice;
                }
                
		return primaSomma + secondaSomma;
	}


    	public final String getCategory() 
	{
		return "Matrice";
	}

  
	public final String getHelp() 
	{
		return "Restituisce la somma della somma dei quadrati dei valori corrispondenti delle due matrici.";
	} 

            
	public final String getName() 
	{
		return "SOMMA.SOMMA.Q";
	}

}
