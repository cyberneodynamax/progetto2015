package it.unica.pr2.progetto2015.g48864_48917;

import it.unica.pr2.progetto2015.interfacce.SheetFunction;

public class Semplice implements SheetFunction 
{
        public Semplice()
        {
        
        };
        
	public Object execute(Object ... args) 
	{
		return 3.14159265358979;
	}

	public final String getCategory() 
	{
		return "Matematica";
	}

	public final String getHelp() 
	{
		return "Restituisce 3,14159265358979, il valore della costante matematica PI con 14 posizioni decimali.";
	} 
       
	public final String getName() 
	{
		return "PI.GRECO";
	}

}
